import { FC, useState, ChangeEvent } from 'react'
import { TableRow } from '../../components'
import { RowData } from '../../types'
import { Button } from '../../components/Button/Button'
import styled from 'styled-components'

const Container = styled.div`
  margin: 2rem;
  width: 100%;
`

const StyledTableContainer = styled.div`
  padding: .75rem;
  border: 1px solid #E2E8F0;
  border-radius: 12px;
  overflow-x: auto;
`

const Table = styled.table`
  font-variant-numeric: lining-nums tabular-nums;
  border-collapse: collapse;
  width: 100%;
`

const TableHeading = styled.th`
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: .05rem;
  text-align: start;
  padding-inline-start: 1.5rem;
  padding-inline-end: 1.5rem;
  padding-top: 0.75rem;
  padding-bottom: 0.75rem;
  line-height: 1.5;
  font-size: 0.75rem;
  color: #4A5568;
  border-bottom-style: solid;
  border-bottom-width: 1px;
  border-color: #EDF2F7;
`

const columns = [
  {
    label: 'Merchant',
    key: 'merchant',
  },
  {
    label: 'Item',
    key: 'item',
  },
  {
    label: 'Amount (Crypto)',
    key: 'amount',
  },
  {
    label: 'Currency',
    key: 'currency',
  },
  {
    label: 'Rate (USD)',
    key: 'rate',
  },
  {
    label: 'Total Amount (USD)',
    key: 'total',
  },
]

export const TableContainer: FC = () => {
  const [rowData, setRowData] = useState<RowData[]>([
    {
      merchant: 'ShirtTown',
      item: 'T-Shirts',
      amount: '1.43219876',
      currency: 'BTC',
    },
    {
      merchant: 'CrazyCups',
      item: 'Cups',
      amount: '1.76236751',
      currency: 'BCH',
    },
    {
      merchant: 'GimmeGold',
      item: 'Gold bullion',
      amount: '10.78654328',
      currency: 'ETH',
    },
  ])
  const [isEditing, setIsEditing] = useState<boolean>(false)

  const handleCellChange = (
    e: ChangeEvent<HTMLInputElement|HTMLSelectElement>,
    type: string,
    index: number,
  ) => {
    const prevState = rowData
    prevState[index] = {
      ...prevState[index],
      [type]: e.target.value
    }
    
    setRowData(prevState)
  }

  const handleAddNew = () => {
    const prevState = rowData
    setRowData([
      ...prevState,
      {
        merchant: '',
        item: '',
        amount: '0',
        currency: 'BTC'
      }
    ])
  }

  const handleDeleteRow = (index: number) => {
    const prevState = rowData
    prevState.splice(index, 1)

    setRowData(prevState)
  }

  return (
    <Container>
      <StyledTableContainer>
        <Table>
          <thead>
            <tr>
              {columns.map(column => (
                <TableHeading scope='column' key={column.key}>
                  {column.label}
                </TableHeading>
              ))}
            </tr>
          </thead>
          <tbody>
            {rowData.map((row: any, index: number) => (
              <TableRow
                key={index}
                merchant={row.merchant}
                item={row.item}
                amount={parseFloat(row.amount)}
                currency={row.currency}
                isEditing={isEditing}
                onCellChange={(e, type) => handleCellChange(e, type, index)}
                onDelete={() => handleDeleteRow(index)}
              />
            ))}
          </tbody>
        </Table>
      </StyledTableContainer>
      {!isEditing ? (
        <Button
          onClick={() => setIsEditing(true)}
          label='Edit'
          background='#0BC5EA'
          color='#fff'
        />
      ) : (
        <>
          <Button
            onClick={handleAddNew}
            label='Add New'
            background='#0BC5EA'
            color='#fff'
          />
          <Button
            onClick={() => setIsEditing(false)}
            label='Save'
            background='#0BC5EA'
            color='#fff'
          />
        </>
      )}
    </Container>
  )
}
