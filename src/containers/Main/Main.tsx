import { FC } from 'react'
import { TableContainer } from '../TableContainer'
import styled from 'styled-components'

const Container = styled.div`
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: start;
`

export const Main: FC = () => {
  return (
    <Container>
      <TableContainer />
    </Container>
  )
}