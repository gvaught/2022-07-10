import { FC } from 'react'
import styled from 'styled-components'

interface ButtonProps {
  label: string
  onClick: () => void
  background?: string
  color?: string
}

const StyledButton = styled.button<any>`
  margin: .1rem;
  border: none;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  position: relative;
  white-space: nowrap;
  vertical-align: middle;
  outline: transparent solid 2px;
  outline-offset: 2px;
  line-height: 1.2;
  border-radius: 0.375rem;
  font-weight: 600;
  transition-property: background-color,border-color,color,fill,stroke,opacity,box-shadow,transform;
  transition-duration: 200ms;
  height: 2.5rem;
  font-size: 1rem;
  padding-inline-start: 1rem;
  padding-inline-end: 1rem;
  background: ${props => props.background};
  color: ${props => props.color};
  &:hover {
    cursor: pointer;
    filter: brightness(85%);
  }
`

export const Button: FC<ButtonProps> = ({
  label,
  onClick,
  background,
  color,
}) => {
  return (
    <StyledButton onClick={onClick} background={background} color={color}>{label}</StyledButton>
  )
}