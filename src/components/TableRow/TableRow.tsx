import { ChangeEvent, FC } from 'react'
import { useExchangeRate } from './../../hooks'
import styled from 'styled-components'
import { Button } from '../Button/Button'

const TableDataCell = styled.td`
  text-align: start;
  padding-inline-start: 1.5rem;
  padding-inline-end: 1.5rem;
  padding-top: 1rem;
  padding-bottom: 1rem;
  line-height: 1.25;
  border-bottom-style: solid;
  border-bottom-width: 1px;
  border-color: #EDF2F7;
  > select, input {
    width: 100%;
    font-size: 1rem;
    padding-inline-start: 1rem;
    outline: transparent solid 2px;
    outline-offset: 2px;
    position: relative;
    transition-property: background-color,border-color,color,fill,stroke,opacity,box-shadow,transform;
    transition-duration: 200ms;
    height: 2.5rem;
    border-radius: 0.375rem;
    border-style: solid;
    border-width: 1px;
    border-image: none 100% / 1 / 0 stretch;
    border-color: inherit;
    background: #fff;
    &:hover {
      border-color: #CBD5E0;
    }
    &:focus-visible {
      z-index: 1;
      border-color: rgb(49, 130, 206);
      box-shadow: rgb(49, 130, 206) 0px 0px 0px 1px;
    }
  }
`

const StyledTableRow = styled.tr`
  &:nth-of-type(2n+1) {
    background-color: #E2E8F0;
  }
`

interface TableRowProps {
  merchant: string
  item: string
  amount: number
  currency: string
  isEditing: boolean
  onCellChange: (
    e: ChangeEvent<HTMLInputElement|HTMLSelectElement>,
    type: string
  ) => void
  onDelete: () => void
}

export const TableRow: FC<TableRowProps> = ({
  merchant,
  item,
  amount,
  currency,
  isEditing,
  onCellChange,
  onDelete,
}) => {
  const { rate, isLoading } = useExchangeRate(currency)

  if (isLoading) {
    return null
  }

  return (
    <StyledTableRow>
      {isEditing ? (
        <>
          <TableDataCell>
            <input
              type='text'
              defaultValue={merchant}
              onChange={(e) => onCellChange(e, 'merchant')}
            />
          </TableDataCell>
          <TableDataCell>
            <input
              type='text'
              defaultValue={item}
              onChange={(e) => onCellChange(e, 'item')}
            />
          </TableDataCell>
          <TableDataCell>
            <input
              type='number'
              step={0.00000001}
              defaultValue={amount.toFixed(8)}
              onChange={(e) => onCellChange(e, 'amount')}
            />
          </TableDataCell>
          <TableDataCell>
            <select
              defaultValue={currency}
              onChange={(e) => onCellChange(e, 'currency')}
            >
              <option value='BTC'>BTC</option>
              <option value='BCH'>BCH</option>
              <option value='ETH'>ETH</option>
              <option value='DOGE'>DOGE</option>
              <option value='SHIB'>SHIB</option>
              <option value='LTC'>LTC</option>
            </select>
          </TableDataCell>
          <TableDataCell>...</TableDataCell>
          <TableDataCell>...</TableDataCell>
          <Button
            onClick={onDelete}
            label='Delete'
            background='red'
            color='#fff'
          />
        </>
      ) : (
        <>
          <TableDataCell>{merchant}</TableDataCell>
          <TableDataCell>{item}</TableDataCell>
          <TableDataCell>{amount.toFixed(8)}</TableDataCell>
          <TableDataCell>{currency}</TableDataCell>
          <TableDataCell>{`$${rate.toLocaleString('en-US')}`}</TableDataCell>
          <TableDataCell>{`$${(amount*rate).toLocaleString('en-US')}`}</TableDataCell>
        </>
      )}
    </StyledTableRow>
  )
}
