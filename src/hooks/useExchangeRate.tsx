import { useMemo, useState } from 'react'
import useSWR from 'swr'

const fetcher = (url: string): Promise<any> =>
  fetch(url).then(resp => resp.json())

export const useExchangeRate = (currency: string) => {
  const [isLoading, setIsLoading] = useState<boolean>(true)
  const { data } = useSWR(
    `https://bitpay.com/api/rates/${currency}/USD`,
    fetcher
  )

  useMemo(() => {
    if (data?.rate) {
      setIsLoading(false)
    }
  }, [data])

  return {
    rate: data?.rate,
    isLoading,
  }
}
