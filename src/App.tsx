import './App.css';
import { Main } from './containers';

function App() {
  return (
    <Main />
  );
}

export default App;
