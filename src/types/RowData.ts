export interface RowData {
  merchant: string
  item: string
  amount: string
  currency: string
}
